import { Component, OnInit } from '@angular/core';
import { empty } from 'rxjs';
import { RestService } from '../rest-service';
import { Utente } from '../utente';

@Component({
  selector: 'app-registrazione',
  templateUrl: './registrazione.component.html',
  styleUrls: ['./registrazione.component.css']
})
export class RegistrazioneComponent implements OnInit {

  inputUsername : string | undefined;
  inputPassword : string | undefined;

  constructor(private rest : RestService) { }

  coloreRegistrazione =false;
  ngOnInit(): void {
  }

  registraUtente() {
    let temp = new Utente();
    temp.username=this.inputUsername;
    temp.pass=this.inputPassword;

    if(temp.username == undefined || temp.username == null ||
      temp.pass == null || temp.pass == undefined ){
        this.coloreRegistrazione =false;

    }else{
      console.log(temp.username +" "+temp.pass);
      this.rest.inserisciUtente(temp).subscribe(
        (risultato) =>{
          if(risultato){
            this.coloreRegistrazione =true;
          }
        },
        (errore) =>{
          console.log(errore);
  
        }
  
      );

    }
  }

}

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Button } from 'selenium-webdriver';
import { RestService } from '../rest-service';
import { Utente } from '../utente';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  coloreAlertLogin=false;
  inputUsernameLogin : string|undefined;
  inputPasswordLogin : string|undefined;

  constructor(private rottaAttiva: ActivatedRoute,
    private rest : RestService,
    private router : Router) { }

  ngOnInit(): void {
  }


  accediButtone(){
    let temp = new Utente();
    temp.username = this.inputUsernameLogin;
    temp.pass = this.inputPasswordLogin;

    if(temp.username == undefined || temp.username == null ||
      temp.pass == null || temp.pass == undefined ){
        this.coloreAlertLogin =false;
        alert("inserisci tutti i campi");

    }else{
      console.log(temp.username +" "+temp.pass);
      this.rest.checkUserLogin(temp).subscribe(
        (risultato) =>{
          if(risultato==true){
            this.coloreAlertLogin =true;
            console.log(risultato);
            
            sessionStorage.setItem("loggato","true");
            sessionStorage.setItem("utente",JSON.stringify(temp));
          }else{
            alert("utente non registrato");
          }
        },
        (errore) =>{
          console.log(errore);
  
        }
  
      );

    }
  }
}
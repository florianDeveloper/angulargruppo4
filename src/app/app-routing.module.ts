import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccountDetailComponent } from './account-detail/account-detail.component';
import { LoginComponent } from './login/login.component';
import { OggettiListComponent } from './oggetti-list/oggetti-list.component';
import { RegistrazioneComponent } from './registrazione/registrazione.component';

const routes: Routes = [
{path: "registrazione", component: RegistrazioneComponent},
{path: "login", component: LoginComponent},
{path: "store", component: OggettiListComponent},
{path: "dettaglio", component: AccountDetailComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

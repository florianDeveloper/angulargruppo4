import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest-service';

@Component({
  selector: 'app-oggetti-list',
  templateUrl: './oggetti-list.component.html',
  styleUrls: ['./oggetti-list.component.css']
})
export class OggettiListComponent implements OnInit {

  elenco : any = [];
  constructor(private rest : RestService) { }
  showlista=false;

  ngOnInit(): void {
    let loggato =sessionStorage.getItem("loggato");
    console.log(loggato);
    if(loggato=="true"){
      this.showlista=true;
      this.rest.getalloggetti().subscribe(
        (responso) => {
          this.elenco = responso;
          console.log(responso);
        }, (errore) => {
          console.log(errore);
        }
      );
    }else{
      alert("devi loggare per poter vedere questa pagina")
    }


    

  }


}

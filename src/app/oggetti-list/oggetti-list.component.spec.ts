import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OggettiListComponent } from './oggetti-list.component';

describe('OggettiListComponent', () => {
  let component: OggettiListComponent;
  let fixture: ComponentFixture<OggettiListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OggettiListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(OggettiListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

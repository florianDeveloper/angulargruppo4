import { Component, OnInit } from '@angular/core';
import { RestService } from '../rest-service';
import { Utente } from '../utente';

@Component({
  selector: 'app-account-detail',
  templateUrl: './account-detail.component.html',
  styleUrls: ['./account-detail.component.css']
})
export class AccountDetailComponent implements OnInit {
  inputUsername: string = "";
  inputPassword:string = "";

  constructor(private rest:RestService){
  }
  utenteInSessione : any = [];
  showDettaglio=false;
  temp: any;

  ngOnInit(): void {
    
    let loggato =sessionStorage.getItem("loggato");
    console.log(loggato);
    if(loggato=="true"){
      this.showDettaglio=true;
      this.temp= sessionStorage.getItem("utente");
      console.log(this.temp);
      this.utenteInSessione=JSON.parse(this.temp);
      console.log(this.utenteInSessione);
      this.inputUsername = this.utenteInSessione.username;
      this.inputPassword = this.utenteInSessione.pass;
    }else{
      alert("devi loggare per poter vedere questa pagina")
    }
  }
  EliminaUtente(){
    let username =this.utenteInSessione.username;
    this.rest.eliminaAccount(username).subscribe(
      (success)=>{
        sessionStorage.setItem("loggato","false");
        console.log(success);
      },
      (errore)=>{
        console.log(errore);

      }
      
    );
  }



  

  
}

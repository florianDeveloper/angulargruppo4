import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { provideRoutes } from "@angular/router";
import { Utente } from "./utente";

const endpoint = "http://localhost:8080/utente";
 const endpointOggetto = "http://localhost:8080/oggetto";

@Injectable({
    providedIn: 'root'
})
export class RestService {

    constructor(private http: HttpClient) { 
        

    }
    inserisciUtente(objUtente : Utente){
        let headers_custom = new HttpHeaders();
        headers_custom = headers_custom.set('Content-Type', 'application/json');
        return this.http.post(endpoint +"/registrazione",JSON.stringify(objUtente),{headers: headers_custom});
    }
    checkUserLogin(objUtente: Utente){
        let headers_custom = new HttpHeaders();
        headers_custom = headers_custom.set('Content-Type', 'application/json');
        return this.http.post(endpoint+"/controllaUtente",JSON.stringify(objUtente),{headers:headers_custom});

    }
    getalloggetti(){
        return this.http.get(endpointOggetto+"/recuperaoggetti");

    }
    eliminaAccount(objUtente: Utente){
        let headers_custom = new HttpHeaders();
        headers_custom = headers_custom.set('Content-Type', 'application/json');
        return this.http.post(endpoint+"/eliminaUtente",objUtente);
    }
}
